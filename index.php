<!doctype html>

<html lang="pt-pt">
    <!-- head -->
        <?php require_once 'includes/head.inc.php'; ?>
    <!-- .head -->

    <body>
        
        <section class="wrapper">
            
            <aside>
                <!-- nav -->
                    <?php require_once 'includes/nav.inc.php'; ?>
                <!-- .nav -->

            </aside>
        </section>

        <section id="content">  
           
            <!-- header -->
            <article id="header">
                <?php require_once 'includes/header.inc.php' ?>
            </article>
            <!-- .header --> 
            
            <button type="button" id="sidebarCollapse" class="btn toggle btn-info ">
                <i class="fas fa-bars"></i>
            </button> 

            <article id="rest">
                <!-- intro -->
                <article id="intro">
                    <?php require_once 'includes/intro.inc.php' ?>
                </article>
                <!-- .intro -->

                <!-- recent -->
                <article id="recent">
                    <?php require_once 'includes/recent.inc.php' ?>
                </article>
                <!-- .recent -->

                <!-- skills -->
                <article id="skills">
                    <?php require_once 'includes/skills.inc.php' ?>
                </article>
                <!-- .skills -->

                <!-- contacts -->
                <article id="contacts">
                    <?php require_once 'includes/contacts.inc.php' ?>
                </article>
                <!-- .contacts -->

                <!-- helpers -->
                <!-- <article id="helpers">
                    <?php require_once 'includes/helpers.inc.php' ?>
                </article> -->
                <!-- .helpers -->
            </article>
        </section>   
        
        <?php require_once 'includes/script.inc.php'; ?>
    </body>
</html>