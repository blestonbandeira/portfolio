<footer>
    
    <p class="signature">&copy;blestonbandeira</p>
    <section class="imgs">
        <a href="https://www.facebook.com/blestonbandeira" target="_blank">
            <div class="imgfacebook"> </div>        
        </a>

        <a href="https://www.instagram.com/blestonbandeira" target="_blank">
            <div class="imginstagram"> </div>
        </a>
        
        <a href="https://twitter.com/blestonbandeir" target="_blank">
            <div class="imgtwitter"> </div>
        </a>

        <a href="https://www.reddit.com/user/blestonbandeira/" target="_blank">
            <div class="imgreddit"> </div>
        </a>

        <a href="https://www.linkedin.com/in/blestonbandeira/" target="_blank">
            <div class="imglinkedin"> </div>
        </a>

        <a href="https://gitlab.com/blestonbandeira" target="_blank">
            <div class="imggit"> </div>
        </a>
        
    </section>
 
</footer>