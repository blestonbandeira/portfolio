<!-- Projects section v.3 -->
<section class="my-5">

  <!-- Section heading -->
  <h1 class="h1-responsive font-weight-bold text-center my-5">Projectos</h1>

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-5 mb-lg-0 mb-5">
      <!--Image-->
      <img src="../img/cabecalho_sepia.jpg" alt="lestonbandeira blog" class="img-fluid rounded z-depth-1">

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-7">

      <!-- Grid row -->
      <div class="row mb-3">
        <div class="col-md-1 col-2">
        </div>
        <div class="col-md-11 col-10">
          <h2 class="font-weight-bold mb-3">Para Lá da Bolha</h2>
          <p class="grey-text">Um olhar sobre o que fica para lá de nós próprios.</p>
          <a href="https://lestonbandeira.wordpress.com/" target="_blank"><button type="button" class="btn btn-dark">Visitar</button></a>
        </div>
      </div>
      <!-- Grid row -->

      <!-- Grid row -->
      <div class="row mb-3">
        <div class="col-md-1 col-2">
       
        </div>
        <div class="col-md-11 col-10">
     
          
        </div>
      </div>
      <!-- Grid row -->

      <!-- Grid row -->
      <div class="row">
        <div class="col-md-1 col-2">
      
        </div>
        <div class="col-md-11 col-10">
          
        </div>
      </div>
      <!-- Grid row -->

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

  <hr class="my-5">

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-7">

      <!-- Grid row -->
      <div class="row mb-3">
        <div class="col-md-1 col-2">
          <i class="far fa-chart-bar fa-2x indigo-text"></i>
        </div>
        <div class="col-md-11 col-10">
          <h2 class="font-weight-bold mb-3">Portfolio</h2>
          <p class="grey-text">Uma visita pelo meu mundo</p>
        </div>
      </div>
      <!-- Grid row -->

      <!-- Grid row -->
      <div class="row mb-3">
        <div class="col-md-1 col-2">
        
        </div>
        <div class="col-md-11 col-10">
         
        </div>
      </div>
      <!-- Grid row -->

      <!-- Grid row -->
      <div class="row mb-lg-0 mb-5">
        <div class="col-md-1 col-2">
         
        </div>
        <div class="col-md-11 col-10">
         
        </div>
      </div>
      <!-- Grid row -->

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-5">
      <!--Image-->
      <img src="https://mdbootstrap.com/img/Photos/Others/images/82.jpg" alt="Sample project image" class="img-fluid rounded z-depth-1">
    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

</section>
<!-- Projects section v.3 -->