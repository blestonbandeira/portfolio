<nav id="sidebar">
    <section class="sidebar-header" id="primeira">    
        <p>portfolio</p>    
        <p>by</p>
        <a href="index.php"><p class="signature">blestonbandeira</p></a>
    </section>

    <section class="caixa" id="segunda">
        <ul class="list-unstyled components">
            

            <li>
                <a href="#intro">Introdução</a>
            </li>
            <li>
                <a href="#recent">Recentes</a>
            </li>
            <li>
                <a href="#skills">Skills</a>
            </li>
            <li>
                <a href="#contacts">Contactos</a>
            </li>
            <li>
                <a href="#helpers">Agradecimentos</a>
            </li>
            
        </ul>
    </section>

    <section class="caixa" id="terceira">
        <ul class="nav flex-sm-column">
            <li class="nav-item">
                <a class="nav-link azul" href="#">
                    
                        <div class="div1"></div>
                        <div class="div2"></div>
                        <div class="div3"></div>
                        <div class="div4"></div>
                        <div class="div5"></div>
                    
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link lilas" href="#">
                
                        <div class="div6"></div>
                        <div class="div7"></div>
                        <div class="div8"></div>
                        <div class="div9"></div>
                        <div class="div10"></div>
                    
                </a>
            </li>
        </ul>
    
    </section>

    <section class="caixa" id="quarta">
        <a class="nav-link pt" href="index.php">pt</a>
        <a class="nav-link en" href="index_en.php">en</a>
    
    </section>

    <section class="caixa">
        <!-- footer -->
            <?php require_once 'includes/footer.inc.php'; ?>
        <!-- .footer -->
    </section>


    
</nav> 