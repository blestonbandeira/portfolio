<section id="helpers">
    <!--Accordion wrapper-->
<div class="accordion md-accordion accordion-1" id="accordionEx23" role="tablist">
  <div class="card">
    <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading96">
      <h5 class="text-uppercase mb-0 py-1">
        <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse96"
          aria-expanded="false" aria-controls="collapse96">
          Bootstrap
        </a>
      </h5>
    </div>
    <div id="collapse96" class="collapse" role="tabpanel" aria-labelledby="heading96"
      data-parent="#accordionEx23">
      <div class="card-body">
        <div class="row my-4">
          <div class="col-md-8">
            <p class="mb-0">Build responsive, mobile-first projects on the web with the world’s most popular front-end component library.</p>
          </div>
          <div class="col-md-4 mt-3 pt-2">
            <div class="view z-depth-1">
                <a href="https://getbootstrap.com/" target="_blank">
                    <img src="../img/bootstrap.png" alt="" class="img-fluid">
                </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading97">
      <h5 class="text-uppercase mb-0 py-1">
        <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse97"
          aria-expanded="false" aria-controls="collapse97">
         ColourLovers
        </a>
      </h5>
    </div>
    <div id="collapse97" class="collapse" role="tabpanel" aria-labelledby="heading97"
      data-parent="#accordionEx23">
      <div class="card-body">
        <div class="row my-4">
          <div class="col-md-8">
            <p class="mb-0">COLOURlovers is a creative community where people from around the world create and share colors, palettes and patterns, discuss the latest trends and explore colorful articles... All in the spirit of love.</p>
          </div>
          <div class="col-md-4 mt-3 pt-2">
            <div class="view z-depth-1">
                <a href="https://www.colourlovers.com/" target="_blank">
                    <img src="../img/colourlovers.png" alt="ColourLovers logo" class="img-fluid">
                </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading98">
      <h5 class="text-uppercase mb-0 py-1">
        <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse98"
          aria-expanded="false" aria-controls="collapse98">
          iconmonstr
        </a>
      </h5>
    </div>
    <div id="collapse98" class="collapse" role="tabpanel" aria-labelledby="heading98"
      data-parent="#accordionEx23">
      <div class="card-body">
        <div class="row my-4">
          <div class="col-md-8">        
            <p class="mb-0">Discover 4432+ free icons in 307 collections</p>
          </div>
          <div class="col-md-4 mt-3 pt-2">
            <div class="view z-depth-1">
                <a href="https://iconmonstr.com/" target=_blank>
                    <img src="../img/iconmonstr.png" alt="iconmonstr logo" class="img-fluid">
                </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading99">
      <h5 class="text-uppercase mb-0 py-1">
        <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse99"
          aria-expanded="false" aria-controls="collapse99">
          Meet The (other) Developer
        </a>
      </h5>
    </div>
    <div id="collapse99" class="collapse" role="tabpanel" aria-labelledby="heading99"
      data-parent="#accordionEx23">
      <div class="card-body">
        <div class="row my-4">
          <div class="col-md-8">
            <p class="mb-0">Bruno Rodrigues</p>
          </div>
          <div class="col-md-4 mt-3 pt-2">
            <div class="view z-depth-1">
                <a href="http://brodriguespt.gitlab.io/projeto-html/" target="_blank">
                    <img src="../img/developer.png" alt="Bruno Rodrigues" class="img-fluid">
                </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading98">
      <h5 class="text-uppercase mb-0 py-1">
        <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse100"
          aria-expanded="false" aria-controls="collapse100">
          Material Design for Bootstrap
        </a>
      </h5>
    </div>
    <div id="collapse100" class="collapse" role="tabpanel" aria-labelledby="heading100"
      data-parent="#accordionEx23">
      <div class="card-body">
        <div class="row my-4">
          <div class="col-md-8">
            <p class="mb-0">The world's most popular framework for building responsive, mobile-first websites and apps</p>
          </div>
          <div class="col-md-4 mt-3 pt-2">
            <div class="view z-depth-1">
                <a href="https://mdbootstrap.com/" target="_blank">
                    <img src="../img/mdbootstrap.png" alt="MDB logo" class="img-fluid">
                </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading101">
      <h5 class="text-uppercase mb-0 py-1">
        <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse101"
          aria-expanded="false" aria-controls="collapse101">
          PlaceIMG
        </a>
      </h5>
    </div>
    <div id="collapse101" class="collapse" role="tabpanel" aria-labelledby="heading101"
      data-parent="#accordionEx23">
      <div class="card-body">
        <div class="row my-4">
          <div class="col-md-4 mt-3 pt-2">
            <div class="view z-depth-1">
                <a href="https://placeimg.com/" target=_blank>
                    <img src="../img/placeimg.png" alt="PlaceIMG logo" class="img-fluid">
                </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
</div>
<!--Accordion wrapper-->

</section>
