<br/>
<h3>webdesign</h3>

<p>html</p>
<section class="progress">
  <section class="progress-bar" style="width:75%">75%</section>
</section>

<p>css</p>
<section class="progress">
  <section class="progress-bar" style="width:65%">65%</section>
</section>

<p>javascript</p>
<section class="progress">
  <section class="progress-bar" style="width:25%">25%</section>
</section>

<br/>
<h3>cms</h3>

<p>wordpress</p>
<section class="progress">
  <section class="progress-bar" style="width:45%">45%</section>
</section>

<p>drupal</p>
<section class="progress">
  <section class="progress-bar" style="width:10%">10%</section>
</section>

<br/>
<h3>programação</h3>

<p>php</p>
<section class="progress">
  <section class="progress-bar" style="width:50%">50%</section>
</section>

<p>sql</p>
<section class="progress">
  <section class="progress-bar" style="width:15%">15%</section>
</section>

<p>c</p>
<section class="progress">
  <section class="progress-bar" style="width:25%">25%</section>
</section>

<p>c++</p>
<section class="progress">
  <section class="progress-bar" style="width:25%">25%</section>
</section>

<p>c#</p>
<section class="progress">
  <section class="progress-bar" style="width:25%">25%</section>
</section>

<br/>
<h3>mobile</h3>

<p>android studio</p>
<section class="progress">
  <section class="progress-bar" style="width:25%">25%</section>
</section>

<p>xamarim</p>
<section class="progress">
  <section class="progress-bar" style="width:35%">35%</section>
</section>

<br/>
<h3>outras</h3>

<p>photoshop</p>
<section class="progress">
  <section class="progress-bar" style="width:10%">10%</section>
</section>

<p>premiere</p>
<section class="progress">
  <section class="progress-bar" style="width:10%">10%</section>
</section>

<p>after effects</p>
<section class="progress">
  <section class="progress-bar" style="width:15%">15%</section>
</section>
