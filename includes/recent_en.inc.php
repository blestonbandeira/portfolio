<section class="card">
  <img class="card-img-top" src="../img/cabecalho_bw.jpg" alt="Card image cap"> 
  <section class="card-body">
    <h5 class="card-title">Para Lá da Bolha</h5>
    <h6>Online since Feb/2017</h6>
    
    <a href="https://lestonbandeira.wordpress.com/" target="_blank" class="btn btn-primary">Go</a>
  </section>
</section>

<section class="card">
  <img class="card-img-top" src="../img/portfolio.jpg" alt="Card image cap">
  <section class="card-body">
    <h5 class="card-title">Porfolio</h5>
    <h6>Online since May/2019</h6>
    <a href="#" class="btn btn-primary">Stick around</a>
  </section>
</section>

<section class="card">
  <img class="card-img-top" src="../img/placeholder_1.jpg" alt="Card image cap">
  <section class="card-body">
    <h5 class="card-title">Soon</h5>
    <h6>Coming Soon</h6>
    <a href="#contacts" class="btn btn-primary">Soon</a>
  </section>
</section> 

<section class="card">
  <img class="card-img-top" src="../img/placeholder_2.jpg" alt="Card image cap">
  <section class="card-body">
    <h5 class="card-title">Soon</h5>
    <h6>Coming Soon</h6>
    <a href="#contacts" class="btn btn-primary">Soon</a>
  </section>
</section> 

<section class="card">
  <img class="card-img-top" src="../img/placeholder_3.jpg" alt="Card image cap">
  <section class="card-body">
    <h5 class="card-title">Soon</h5>
    <h6>Coming Soon</h6>
    <a href="#contacts" class="btn btn-primary">Soon</a>
  </section>
</section> 

<section class="card">
  <img class="card-img-top" src="../img/placeholder_4.jpg" alt="Card image cap">
  <section class="card-body">
    <h5 class="card-title">Soon</h5>
    <h6>Coming Soon</h6>
    <a href="#contacts" class="btn btn-primary">Soon</a>
  </section>
</section> 

<section class="card">
  <img class="card-img-top" src="../img/placeholder_5.jpg" alt="Card image cap">
  <section class="card-body">
    <h5 class="card-title">Soon</h5>
    <h6>Coming Soon</h6>
    <a href="#contacts" class="btn btn-primary">Soon</a>
  </section>
</section> 

<section class="card">
  <img class="card-img-top" src="../img/placeholder_6.jpg" alt="Card image cap">
  <section class="card-body">
    <h5 class="card-title">Soon</h5>
    <h6>Coming Soon</h6>
    <a href="#contacts" class="btn btn-primary">Soon</a>
  </section>
</section> 