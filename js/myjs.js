$(document).ready(function () {
  $(".sidebar").mCustomScrollbar({
      theme: "minimal"
  });

  $('#sidebarCollapse').on('click', function () {
      $('#sidebar, #content').toggleClass('active');
      $('.collapse.in').toggleClass('in');
      $('a[aria-expanded=true]').attr('aria-expanded', 'false');
  });


  $('.azul').click(function(){
    $('body').removeClass().addClass('azul');
  });
  $('.lilas').click(function(){
    $('body').removeClass().addClass('lilas');
  });
});

var ctxL = document.getElementById("lineChart1").getContext('2d');
var myLineChart = new Chart(ctxL, {
  type: 'line',
  data: {
    labels: ["8 AM", "10 AM", "12 PM", "2 PM", "4 PM", "6 PM", "8 PM"],
    datasets: [{
      fill: false,
      borderColor: "#673ab7",
      pointBackgroundColor: "#673ab7",
      data: [885, 884, 887, 883, 888, 889, 888]
    }]
  },
  options: {
    responsive: false,
    legend: {
      display: false
    },
    elements: {
      line: {
        tension: 0.0
      }
    },
    scales: {
      xAxes: [{
        gridLines: {
          display: false,
        },
        ticks: {
          padding: 15,
          height: 30
        }
      }],
      yAxes: [{
        gridLines: {
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 5,
          padding: 15,
          min: 880,
          max: 890
        }
      }]
    }
  }
});